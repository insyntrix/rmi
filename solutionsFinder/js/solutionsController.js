angular.module('app').controller('solutionsController', function($scope, $http) {
            
            $http.get('js/data.json')
                .then( function(res){
                   $scope.material = res.data.material;
                   $scope.marking = res.data.marking;       
                   $scope.laser = res.data.laser;
                   $scope.process = res.data.process;
                   $scope.enclosure = res.data.enclosure;
                  
            

                    _jsonTranslator( res );


                    });

           
              

            var _jsonTranslator = function(res) {
                      var jsonData = res.data;

                    //for the more info buttons
                    $scope.alert = function(info){
                    alert(info);
                    };
              

             
               
               

                   //holds the marking choices you have based on the material you selected
                 $scope.markingChoices = function(id){                
                 for ( var q = 0; q < $scope.material[id].marking.length; q ++){ 
                   for( var x= 0; x< $scope.marking.length; x++){                       
                      if($scope.material[id].marking[q] === $scope.marking[x].id){
                         $scope.markingC.push($scope.marking[x]);           
                                  
                  }}}};
                   $scope.markingC = []; 

                  //holds the marking choices you have based on the material you selected
                 $scope.laserChoices = function(idL){                 
                 for ( var z = 0; z < $scope.marking[idL].laser.length; z ++){ 
                   for( var y= 0; y< $scope.laser.length; y++){ 
                      if($scope.marking[idL].laser[z] === $scope.laser[y].id){
                        $scope.laserC.push($scope.laser[y]); 
                        
                                        
                                
                  }}}};
                  $scope.laserC = []; 
                //holds the enclosure choices you have based on the process you selected
                $scope.enclosureChoices = function(idP){                 
                 for ( var p = 0; p < $scope.process[idP].enclosure.length; p ++){ 
                   for( var e= 0; e< $scope.enclosure.length; e++){ 
                      if($scope.process[idP].enclosure[p] === $scope.enclosure[e].id){
                        $scope.enclosureC.push($scope.enclosure[e]); 
                       
                        
                    }}}};
                    $scope.enclosureC = []; 


              

                   //holds selected material
                   $scope.materialChoice = function(choiceMaterial){ 
                   for ( var h = 0; h < $scope.material.length; h ++){
                     if(choiceMaterial === $scope.material[h].id){
                    $scope.materialS = $scope.material[h];
                     
                  }}};
               
                         
          
                    //holds selected marking
                  $scope.markingChoice = function(choiceMarking){ 
                   for ( var i = 0; i < $scope.marking.length; i ++){
                        if(choiceMarking === $scope.marking[i].id){
                    $scope.markingS = $scope.marking[i];
                
                  }}};

                    //holds selected laser
                  $scope.laserChoice = function(choiceLaser){ 
                   for ( var j = 0; j < $scope.laser.length; j ++){
                     if(choiceLaser === $scope.laser[j].id){
                    $scope.laserS = $scope.laser[j];  
                
                  }}};

                    //holds selected process
                   $scope.processChoice = function(choiceProcess){ 
                   for ( var k = 0; k < $scope.process.length; k ++){
                        if(choiceProcess === $scope.process[k].id){
                    $scope.processS = $scope.process[k];

                   }}};

                     //holds selected enclosure
                   $scope.enclosureSelector = function(selectionEnclosure){
                   $scope.enclosureS = selectionEnclosure;
                   
                   };

          



            
            
         }
     });
