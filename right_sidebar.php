<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>

	<div id="banner-wrap">
		<div class="row">
			<h1> <?php echo $c->getCollectionName()?></h1>
		</div>
  </div><!--end. banner-wrap-->
		

	<div class="row" style="margin-top: 30px;">
		<div class="small-12 medium-9 columns">
			<?php
				$areaMain  = new Area('mainContent');
				$areaMain->display($c);
			?>
		</div>
	
		<div class="small-12 medium-3 columns">
			<div class="sidebar">
				<?php
					$areaMain  = new Area('Sidebar');
					$areaMain->display($c);
				?>
			</div>
			<?php 	
				$a = new Area('Sidebar Content');
				$adump = $a->getAreaLayouts($c);
				if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
					echo '<div class="sidebar-content">';
				    $a->display($c);
					echo '</div>';
				}
			?>
		</div><!--end .sml-12 med-3-->
	</div><!--end .row-->

    

<?php $this->inc('elements/footer.php');?>
