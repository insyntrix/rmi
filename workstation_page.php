<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>

		<div id="banner-wrap">
			<div class="row">
				<div class=" small-12 columns">	
					<h1>    <?php $page = Page::getByID($c->getCollectionParentID()); print $page->getCollectionName();?>: <?php echo $c->getCollectionName()?></h1>

				</div>  
			</div>
		</div><!--end .banner-wrap-->
		
		<!--PRODUCT GALLERY SECTION-->
		<section>
			<div class="ProductGallery">
				<div class="row"> 
					<div class=" small-12 columns">
                        <?php
						$areaMain  = new Area('gallery');
						$areaMain->display($c);
					    ?>
					</div><!--end .sml-12-->
				</div><!--end .row-->
			</div><!--end .ProductGallery-->
		</section>
		<div class="row">
			<div class="small-12 medium-6 columns">
				<div id="breadcrumb-wrap">
				<?php
					$nav = BlockType::getByHandle('autonav');
					$nav->controller->orderBy = 'display_asc';
					$nav->controller->displayPages = 'top';
					$nav->controller->displaySubPages = 'relevant_breadcrumb';
					$nav->controller->displaySubPageLevels = 'enough';
					$nav->render('templates/rmi_breadcrumb');
				?>
				</div>
			</div>			
		</div><!--end .row-->
		<br/><br/>
		<!--TABS AREA-->
		<div class="row">
			<div class="small-12 large-12 columns">
				<section id="infoBox">
					<div class="tabsArea">
                        <?php
						$areaMain  = new Area('tabsBox');
						$areaMain->display($c);
					    ?>
					</div><!--end .tabsArea-->
					<div class="request-button">
						<a class="red-btn" href="<?php echo $this->url('/contact-us/request-quote')?>">Request A Quote</a>
					</div>
				</section>
			</div><!--end .sml-12-->
		</div><!--end .row-->
		<div class="row ">
	       <div class="angle-red" style="margin-right:20%">
				<h4>Learn More About this Product</h4>
			</div>
		</div>
		<section id="learnMore">
			<div class="row">
								<?php 	
					$a = new Area('Collateral');
					$adump = $a->getAreaLayouts($c);
					if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
						echo '<div class="small-12 medium-6 columns">';
						echo '<div class="learnMore-inner">';
					    $a->display($c);
					    echo '</div>';
						echo '</div>';
					}
				?>
				
				<div class="small-12 medium-6 columns">
					<div class="learnMore-inner">
						<h3 class="LM-title-sml">Ask Us</h3>
						<h1 class="LM-title-lg">Questions</h1>
						<p><img src= "<?php echo $this->getThemePath();?>/img/question-icon.png"></p>
						<a class="red-btn-outline" href="<?php echo $this->url('/contact-us/request-information');?>">Ask</a>
					</div>
				</div>	
			</div>
		</section>

		<section id="">
			<div class="row">
				 <div class=" small-12 columns">
					<h1>View Any of Our Speciality Workstations</h1>
				</div>
			</div>
			<div class="row" data-equalizer>
				<?php 	
					$a = new Area('Specialty Enclosure 1');
					$adump = $a->getAreaLayouts($c);
					if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
						echo '<div class="small-12 medium-3 columns">';
						echo '<div class="viewLasers" data-equalizer-watch>';
					    $a->display($c);
						echo '</div>';
						echo '</div>';
					}
				?>	
				
				<?php 	
					$a = new Area('Specialty Enclosure 2');
					$adump = $a->getAreaLayouts($c);
					if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
						echo '<div class="small-12 medium-3 columns">';
						echo '<div class="viewLasers" data-equalizer-watch>';
					    $a->display($c);
						echo '</div>';
						echo '</div>';
					}
				?>
				
				<?php 	
					$a = new Area('Specialty Enclosure 3');
					$adump = $a->getAreaLayouts($c);
					if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
						echo '<div class="small-12 medium-3 columns">';
						echo '<div class="viewLasers" data-equalizer-watch>';
					    $a->display($c);
						echo '</div>';
						echo '</div>';
					}
				?>
				
				<?php 	
					$a = new Area('Specialty Enclosure 4');
					$adump = $a->getAreaLayouts($c);
					if (($a->getTotalBlocksInArea($c) > 0) || !empty($adump) || ($c->isEditMode()) ) {
						echo '<div class="small-12 medium-3 columns">';
						echo '<div class="viewLasers" data-equalizer-watch>';
					    $a->display($c);
						echo '</div>';
						echo '</div>';
					}
				?>	
			</div><!--end .row-->
		</section>


<?php $this->inc('elements/footer.php');?>
