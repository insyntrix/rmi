<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php  $this->inc('elements/header.php'); ?>
	<div id="banner-wrap">
		   <div class="row">
			   <div class="small-12 columns">
			  <h1><?php echo $c->getCollectionName()?></h1>
			   </div>
			</div>
		</div><!--end .banner-wrap-->
	<div class="main-body">
		<div class="row">	
			<div class="small-12  columns">
				<div class="row">
					<div class="small-12 medium-6 small-centered large-centered columns">
					<?php Loader::element('system_errors', array('error' => $error)); ?>
					</div>
				</div>
				<br/><br/>
				<?php  print $innerContent; ?>
			</div><!--end .sml-12 .med-8-->
		</div><!--end .row-->
	</div><!--end .main-body-->
<?php   $this->inc('elements/footer.php'); ?>