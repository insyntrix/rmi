<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>
		
			<div id="banner-wrap">
		<div class="row">
		<div class=" small-12 columns">	
			
			            <?php
						$areaMain  = new Area('pageTitle');
						$areaMain->display($c);
					        ?>

			</div>  
			</div>
		</div><!--end .banner-wrap-->
		
		

        <div class="row">
        <div class="small-12 columns">

	                     
					        <?php
						$areaMain  = new Area('mainContent');
						$areaMain->display($c);
					        ?>

		 </div>
        </div>


<section id="basicGrey">

        <div class="row">
		    <div class=" small-8 columns">
			  <div class="angle-red">
		               <?php
						$areaMain  = new Area('boxtitle1');
						$areaMain->display($c);
				    	?>
		     </div>
		   </div>
		</div>

<div class="row">
	<div class="small-12 columns">
	<div class="outlineBox">
	                    <?php
						$areaMain  = new Area('boximage1');
						$areaMain->display($c);
				    	?>
		<div class="left"><?php
						$areaMain  = new Area('boxsubtitle1');
						$areaMain->display($c);
					?></div>
		<div class="basicText">
		<?php
						$areaMain  = new Area('boxcontent1');
						$areaMain->display($c);
					?>
		</div>
		</div>
	</div>
</div>

 <div class="row">
		    <div class=" small-8 columns">
			  <div class="angle-red">
		               <?php
						$areaMain  = new Area('boxtitle2');
						$areaMain->display($c);
				    	?>
		     </div>
		   </div>
		</div>

<div class="row">
	<div class="small-12 columns">
	<div class="outlineBox">
	                    <?php
						$areaMain  = new Area('boximage2');
						$areaMain->display($c);
				    	?>
		<div class="left"><?php
						$areaMain  = new Area('boxsubtitle2');
						$areaMain->display($c);
					?></div>
		<div class="basicText">
		                <?php
						$areaMain  = new Area('boxcontent2');
						$areaMain->display($c);
				     	?>
		</div>
		</div>
	</div>
</div>


 <div class="row">
		    <div class=" small-8 columns">
			  <div class="angle-red">
		               <?php
						$areaMain  = new Area('boxtitle3');
						$areaMain->display($c);
				    	?>
		     </div>
		   </div>
		</div>

<div class="row">
	<div class="small-12 columns">
	<div class="outlineBox">
	                    <?php
						$areaMain  = new Area('boximage3');
						$areaMain->display($c);
				    	?>
		<div class="left"><?php
						$areaMain  = new Area('boxsubtitle3');
						$areaMain->display($c);
					?></div>
		<div class="basicText">
		                <?php
						$areaMain  = new Area('boxcontent3');
						$areaMain->display($c);
				     	?>
		</div>
		</div>
	</div>
</div>

<div class="row">
		    <div class=" small-8 columns">
			  <div class="angle-red">
		               <?php
						$areaMain  = new Area('boxtitle4');
						$areaMain->display($c);
				    	?>
		     </div>
		   </div>
		</div>

<div class="row">
	<div class="small-12 columns">
	<div class="outlineBox">
	                    <?php
						$areaMain  = new Area('boximage4');
						$areaMain->display($c);
				    	?>
		<div class="left"><?php
						$areaMain  = new Area('boxsubtitle4');
						$areaMain->display($c);
					?></div>
		<div class="basicText">
		                <?php
						$areaMain  = new Area('boxcontent4');
						$areaMain->display($c);
				     	?>
		</div>
		</div>
	</div>
</div>





</section><!--end basic grey section-->

<?php $this->inc('elements/footer.php');?>
