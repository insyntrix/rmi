<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>

	<div id="banner-wrap">
		<div class="row">
		<h1><?php echo $c->getCollectionName()?></h1>
		</div>
	</div>
	
	<div class="row">
		<div class="small-12 medium-3 columns">
	
			<div class="small-12 columns">
				<?php
					$areaMain  = new Area('mainImage');
					$areaMain->display($c);
				?>
			</div><!--end .sml-12-->
			<?php
				$areaMain  = new Area('sideContent');
				$areaMain->display($c);
			?>
		</div><!--end .sml-12 med-3-->
	
		<div class="small-12 medium-9 columns">
			<?php
				$areaMain  = new Area('mainContent');
				$areaMain->display($c);
			?>
		</div><!--end .sml-12 med-9-->
	</div><!--end .row-->
 
<?php $this->inc('elements/footer.php');?>