<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>

		<div id="banner-wrap">
		   <div class="row">
			   <div class="small-12 columns">
				   <h1><?php echo $c->getCollectionName()?></h1>
			   </div>
			</div>
		</div><!--end .banner-wrap-->
		<section id="fullpage">
	        <div class="row">
		        <div class="small-12 columns">
	                <section id="Solutionfinder">
						<div class="solution-wrap">
								<div class="sf-container">
									<div class="row">
										<div class="small-12 columns">
											<div class="sf-wrapper clearfix">
												<?php include ('solutionsFinder/mainSolution.html');?>
											</div>
										</div>
									</div>
								</div>
						</div>
					</section>
					
	        	</div>
	        </div><!--end row-->
<!-- 	        <span data-tooltip aria-haspopup='true' class='has-tip' title='Hello tooltip'><i class='fi-info'></i></span> -->
		</section>
<?php $this->inc('elements/footer.php');?>
