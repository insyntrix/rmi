<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!DOCTYPE html>
<html  lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo $this->getThemePath(); ?>/css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $this->getThemePath(); ?>/css/main.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo $this->getThemePath(); ?>/css/style.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo $this->getThemePath(); ?>/css/foundation.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo $this->getThemePath(); ?>/typography.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo $this->getThemePath(); ?>/fonts/foundation-icons.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo $this->getThemePath(); ?>/css/sf-styles.css">
		
		
		<script src="<?php echo $this->getThemePath(); ?>/js/vendor/modernizr.js"></script>
		<?php  Loader::element('header_required'); ?>
		<?php if($c->isEditMode()){ ?>
		<style>body{ position:static; }</style><?php } ?> 
		
		<!--<script>//THIS PREVENTS THE PARENT LINK FROM BEING CLICKABLE
			$(document).ready(function(){
				$('a.has-dropdown').click(function(e) {
					e.preventDefault();
				});
			});	
		</script>-->
	</head>
	<body>
		<header>
			<div class="row hide-for-small-only">
				<div class="small-12 medium-6 columns ">
					<a href="<?php echo DIR_REL?>/"><img src= "<?php echo $this->getThemePath();?>/img/RIM-logo.jpg"></a>
				</div><!--end .sml-12-->	
				<div class="small-12 medium-6 columns">
					<span class="rmiphone"><a href="tel:303-664-9000">+1.303.664.9000</a></span>
				</div><!--end .sml-12-->		
			</div><!--end .row-->
			<div class="row">
				<div class="small-12 columns">
					<?php
						$a = new GlobalArea('Auto Nav');
						$a->setBlockLimit(1);
						$a->display($c);
					?>
				</div><!--end .sml-12-->			
			</div><!--end .row-->
		</header>
		