<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
		<footer>
			<div class="row">
				<div class="small-12 medium-3 columns hide-for-small-only">
					<img src="<?php echo $this->getThemePath();?>/img/RIM-logo.png"/>
					<br/>
					<br/>
					<ul class="social-list">
						<li><a href="https://www.facebook.com/rmilaserllc?_rdr=p" target="_blank"><img src="<?php echo $this->getThemePath();?>/img/facebook.png"/></a></li>
						<li><a href="https://twitter.com/rmi_laser" target="_blank"><img src="<?php echo $this->getThemePath();?>/img/twitter.png"</a></li>
						<li><a href="https://www.linkedin.com/company/rmi-laser" target="_blank"><img src="<?php echo $this->getThemePath();?>/img/linkedin.png"</a></li>
						<li><a href="https://plus.google.com/+Rmilaser/videos" target="_blank"><img src="<?php echo $this->getThemePath();?>/img/google+.png"</a></li>
					</ul>
					<br/>
				</div><!--end .sml-12 med-3-->	
				<div class="small-12 medium-3 columns">
					<div class="contact-info-wrap">
						<address><a href="https://goo.gl/maps/47ylA" target="_blank">106 Laser Drive Bldg #2<br/> Lafayette, CO 80026</a></address>
						<div class="phone-wrap">
						<span><a href="tel:+1-866--952-7370">+1.866.952.7370</a></span><br/>
						<span><a href="tel:+1-303-664-9000">+1.303.664.9000</a></span>
						</div>
						<span class="email"><a href="mailto:info@rmilaser.net" >info@rmilaser.net</a></span>
					</div><!--end .contact-info-wrap-->
				</div><!--end .sml-12 med-3-->
				<div class="small-12 medium-2 columns hide-for-small-only">
					<div class="footer-nav">
						<h6>Applications</h6>
						<?php
							$areaMain = new GlobalArea('Application Footer Nav');
							$areaMain->setBlockLimit(1);
							$areaMain->display($c);
						?>
					</div>
				</div><!--end .sml-12 med-3-->
				<div class="small-12 medium-2 columns hide-for-small-only">
					<div class="footer-nav">
						<h6>The Company</h6>
						<?php
							$areaMain = new GlobalArea('Company Footer Nav');
							$areaMain->setBlockLimit(1);
							$areaMain->display($c);
						?>
					</div>
				</div><!--end .sml-12 med-3-->	
				<div class="small-12 medium-2 columns hide-for-small-only">
					<?php
						$areaMain = new GlobalArea('Made in USA');
						$areaMain->setBlockLimit(1);
						$areaMain->display($c);
					?>
				</div>
			</div><!--end .row-->
		</footer>
		
		<!--JS FILES-->
		
        <script src="<?php echo $this->getThemePath();?>/js/foundation/foundation.js"></script>
        <script src="<?php echo $this->getThemePath();?>/js/foundation/foundation.topbar.js"></script> 
        <script src="<?php echo $this->getThemePath();?>/js/foundation/foundation.reveal.js"></script>
        <script src="<?php echo $this->getThemePath();?>/js/foundation/foundation.equalizer.js"></script>
		<script src="<?php echo $this->getThemePath();?>/js/foundation/foundation.tooltip.js"></script>
		
	
		<?php Loader::element('footer_required') ?>
	    <script>
	      $(document).foundation();
	      $('#ResultId0').mouseenter(function(){
		      $(this).foundation('tooltip');
	      });
	    </script>
	</body>
</html>
