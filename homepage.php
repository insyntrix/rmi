<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>

		<div id="banner-wrap" class="homeBannerWrap">
			<div class="banner">
				<div class="CTA-wrap">
					<h1 class="CTA">The Leader in Marking Lasers<br/> and Equipment</h1>
					<a href="<?php echo $this->url('/contact-us/request-quote/');?>" class="home-red-btn">REQUEST QUOTE</a>
				</div>
				<video autoplay loop poster="homebanner-poster.png" id="bgvid">
					<source src="<?php echo $this->getThemePath();?>/videos/RMI-home-banner-5.mp4.mp4" type="video/mp4">
					<source src="<?php echo $this->getThemePath();?>/videos/RMI-home-banner-5.webmhd.webm" type="video/webm">
					<source src="<?php echo $this->getThemePath();?>/video/RMI-home-banner-5.oggtheora.ogv" type="video/ogg"/>
			</video>
			</div>
		</div><!--end .banner-wrap-->
		
		<section id="laser-experts">
			<div class="row">
				<div class="home-laser-content">
<!--PROMO
					<div style="margin-top: 10px; text-align: center;"><a style="margin-bottom:10px; display: inline-block;" href="<?php echo DIR_REL?>/contact-us/request-quote"><img src="<?php echo $this->getThemePath();?>/img/Year-End-Sale-Banner.jpg" alt="Year End Special - Use it before you loose it! "/></a>
					 </div>
-->
						<h1>Laser Experts</h1>
						<?php
							$areaMain  = new Area('Intro Content');
							$areaMain->display($c);
						?>
					</div><!--end .sml-12 lg-6-->
				</div><!--end .sml-12-->	
			</div><!--end .row-->
		</section>
		
<!--TAKEN OUT FOR NOW UNTIL THEY DECIDE WHAT THEY WANT TO DO
		<section id="Solutionfinder">
			<div class="solution-wrap">
					<div class="sf-container">
						<div class="row">
							<div class="small-12 columns">
								<div class="sf-wrapper clearfix">
									<?php include ('solutionsFinder/mainSolution.html');?>
								</div>
							</div>
						</div>
					</div>
			</div>
		</section>
-->

		<section id="ft-products" >
			<div class="row" data-equalizer data-equalizer-mq="medium-up">
				<div class="small-12 medium-3 columns">
					<div class="feature-wrap" data-equalizer-watch>
						<div class="ft-img ">
							<center>
									<?php
								$areaMain  = new Area('featurePicture1');
								$areaMain->display($c);
					                ?>
							</center>
						</div><!--end .ft-img-->
						<div class="ft-title">
							<h3>Laser Marking Systems</h3>
						</div><!--end .ft-title-->
						<div class="ft-content">
							<?php
								$areaMain  = new Area('featureContent1');
								$areaMain->display($c);
					        ?>
						</div><!--end .ft-content-->
					</div><!--end .feature-wrap-->
				</div><!--end .sml-12-->
				
				
				<div class="small-12 medium-3 columns">
					<div class="feature-wrap" data-equalizer-watch>
						<div class="ft-img">
							<center>
							<?php
								$areaMain  = new Area('featurePicture2');
								$areaMain->display($c);
					                ?>
							</center>
						</div><!--end .ft-img-->
						<div class="ft-title">
							<h3>Laser Workstations</h3>
						</div><!--end .ft-title-->
						<div class="ft-content">
							<?php
								$areaMain  = new Area('featureContent2');
								$areaMain->display($c);
							?>
						</div><!--end .ft-content-->
					</div><!--end .feature-wrap-->
				</div><!--end .sml-12-->

				
				<div class="small-12 medium-3 columns" >
					<div class="feature-wrap" data-equalizer-watch>
						<div class="ft-img">
							<center>
							<?php
								$areaMain  = new Area('featurePicture3');
								$areaMain->display($c);
					                ?>
							</center>
						</div><!--end .ft-img-->
						<div class="ft-title">
							<h3>Accessories</h3>
						</div><!--end .ft-title-->
						<div class="ft-content">
							<?php
								$areaMain  = new Area('featureContent3');
								$areaMain->display($c);
							?>
						</div><!--end .ft-content-->
					</div><!--end .feature-wrap-->
				</div><!--end .sml-12-->		
				
				<div class="small-12 medium-3 columns" >
					<div class="feature-wrap" data-equalizer-watch>
						<div class="ft-img">
							<center>
							<?php
								$areaMain  = new Area('feature Picture 4');
								$areaMain->display($c);
					                ?>
							</center>
						</div><!--end .ft-img-->
						<div class="ft-title">
							<h3>Custom Solutions</h3>
						</div><!--end .ft-title-->
						<div class="ft-content">
							<?php
								$areaMain  = new Area('feature Content 4');
								$areaMain->display($c);
							?>
						</div><!--end .ft-content-->
					</div><!--end .feature-wrap-->
				</div><!--end .sml-12-->
			</div><!--end .row-->
		</section>


		<section id="">
			<div class="row">
				<div class="small-12 medium-4 columns">
					<div class="angle-red">
						<h4>Latest News</h4>
					</div><!--end .angle-->
					<div class="events-wrap">
						<?php
							$areaMain  = new Area('Events');
							$areaMain->display($c);
					    ?>
					</div><!--end .events-wrap-->
					<div class="signup-wrap">
						<div class="angle-red">
							<h4>Newsletter Signup</h4>
						</div>
						<div class="events-wrap">
							<?php
								$areaMain=new Area('Signup');
								$areaMain->display($c);
							?>
						</div>
					</div><!--end .signup-wrap-->
				</div><!--end .sml-12 med-4-->
				<div class="small-12 medium-8 columns show-for-medium-up">
					<div class="row display">
						<div class="small-12 columns">
							<div class="angle-gray">
								<h4>Thousands of Applications</h4>
							</div>
						</div><!--end .sml-12-->
					</div><!--end .row-->
					<div class="row display">
						<div class="medium-6 columns">
							<div class="app-img-wrap">
								<span class="app-name">Stainless Steel</span>
								<a href="<?php echo $this->url('/applications/materials');?>"><img src= "<?php echo $this->getThemePath();?>/img/material/stainlesssteal-ft.jpg"></a>		
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="app-img-wrap">
								<span class="app-name">Aluminum</span>
								<a href="<?php echo $this->url('/applications/materials');?>"><img src= "<?php echo $this->getThemePath();?>/img/material/aluminium-ft.jpg"></a>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="app-img-wrap">
								<span class="app-name">Chrome</span>
								<a href="<?php echo $this->url('/applications/materials');?>"><img src= "<?php echo $this->getThemePath();?>/img/material/chrome-ft.jpg">	</a>
							</div>
						</div>
					</div><!--end .row-->
				</div><!--end .sml-12 med-8-->
			</div><!--end .row-->
		</section>
		
<?php $this->inc('elements/footer.php');?>