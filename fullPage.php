<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>

		<div id="banner-wrap">
		   <div class="row">
			   <div class="small-12 columns">
				   <h1><?php echo $c->getCollectionName()?></h1>
			   </div>
			</div>
		</div><!--end .banner-wrap-->
		<section id="fullpage">
	        <div class="row">
		        <div class="small-12 columns">
	                <?php
						$areaMain  = new Area('mainContent');
						$areaMain->display($c);
				    ?>
	        	</div>
	        </div><!--end row-->
		</section>
<?php $this->inc('elements/footer.php');?>
