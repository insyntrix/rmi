<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>
        
        
<div id="banner-wrap">
	<div class="row">
		<div class="small-12 columns">
			<h1><?php echo $c->getCollectionName()?></h1>
		</div>
	</div>
</div>
<section id="twocol">
	<div class="row">
		<div class="small-12 columns">
			<?php
				$areaMain  = new Area('mainContent');
				$areaMain->display($c);
			?>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="small-12 columns">
			<h2>Learn more about RMI laser marking systems</h2>
		</div><!--end .sml-12-->
	</div>
	<div class="row" data-equalizer data-equalizer="foo">
	    <div class="small-12 medium-6 large-3 columns">
			<?php
				$areaMain=new Area('Laser 1');
				$areaMain->setBlockLimit(1);
				$areaMain->display($c);
			?>
		</div> <!--sml-12 med-3-->
	
		<div class="small-12 medium-6 large-3 columns">		
			<?php
				$areaMain=new Area('Laser 2');
				$areaMain->setBlockLimit(1);
				$areaMain->display($c);
			?>
		</div> <!--sml-12 med-3-->
		<div class="small-12 medium-6 large-3 columns">		
			<?php
				$areaMain=new Area('Laser 3');
				$areaMain->setBlockLimit(1);
				$areaMain->display($c);
			?>
		</div> <!--sml-12 med-3-->
		<div class="small-12 medium-6 large-3 columns" >		
			<?php
				$areaMain=new Area('Laser 4');
				$areaMain->setBlockLimit(1);
				$areaMain->display($c);
			?>
		</div> <!--sml-12 med-3-->
	</div><!--end .row-->
	
</section>

		
			<div class="laser-CTA">
			<?php
				$areaMain=new Area('Laser CTA');
				$areaMain->display($c);	
			?>
			</div>
					


<?php $this->inc('elements/footer.php');?>
