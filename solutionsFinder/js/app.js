
(function(){
  var app = angular.module('../solutionsFinder', [ ]);

  app.controller('solutionsController', function(){
    this.material = material;
     this.markingT = marking;
 
//to get marking selection to loop selection through the laser array
  //  for(var i =0; i< marking.length; i++){
    //   if (material.marking===marking[i].name){
     //      var laserChoices === $scope.marking[i].laser
      //     console.log($scope.marking[i].laser)
      // };
   // };
//end

 });

  var material = [
    
    { name: 'Aluminum',
    photo:'aluminum',
     info:'',
     marking:'etch'
   },

    { name: 'Anodized Aluminum',
     photo:'aluminum',
     info:' a silver-white metallic element, light in weight, ductile, malleable, and not readily corroded or tarnished, occurring combined in nature in igneous rock, shale, clay, and most soil: used in alloys and for lightweight utensils, castings, airplane parts, etc. ',
       marking: ['ablation','etch','engrave' ]

   },

    { name: 'Brass & Copper',
     photo:'brass',
     info:'',
       marking: ['anneal','etch','engrave' ]
    },

    { name: 'Carbide',
     photo:'aluminum',
      info:'',
       marking: ['anneal','etch','engrave' ]
      },

    { name: 'Ceramic',
     photo:'aluminum',
      info:'',
    marking: ['etch','engrave' ]
     },

    { name: 'Coated & Painted Metals',
     photo:'aluminum', 
      info:'',
      marking: ['ablation','etch','engrave' ]
     },

    { name: 'Fiberglass & Carbon Fiber',
     photo:'aluminum',
      info:'',
      marking: ['etch','engrave']
     },

    { name: 'Nickel',
     photo:'aluminum',
      info:'',
       marking: ['anneal','etch' ] 
    },

    { name: 'Plastics',
     photo:'aluminum',
      info:'any of a group of synthetic or natural organic materials that may be shaped when soft and then hardened, including many types of resins, resinoids, polymers, cellulose derivatives, casein materials, and proteins',
       marking: ['foaming','etch','colorChange' ] 
   },

    { name: 'Rubber',
     photo:'aluminum',
      info:'a material made by chemically treating and toughening this substance, valued for its elasticity, nonconduction of electricity, shock absorption, and resistance to moisture, used in the manufacture of erasers, electrical insulation, elastic bands, crepe soles, toys, water hoses, tires, and many other products. ',
       marking: ['etch','engrave' ]
   },

    { name: 'Semiconductor',
     photo:'aluminum',
      info:'a substance, as silicon or germanium, with electrical conductivity intermediate between that of an insulator and a conductor : a basic component of various kinds of electronic circuit element ',
       marking: ['ablation','colorChange','burnish' ]
   },

    { name: 'Silver and Gold',
     photo:'aluminum',
      info:'siler is a white, ductile metallic element, used for making mirrors, coins, ornaments, table utensils, photographic chemicals, conductors, etc; gold is a precious yellow metallic element, highly malleable and ductile, and not subject to oxidation or corrosion.',
       marking: ['anneal','etch' ]
   },

    { name: 'Stainless Steel',
     photo:'aluminum',
      info:'alloy steel containing 12 percent or more chromium, so as to be resistant to rust and attack from various chemicals. ',
       marking: ['anneal','etch','engrave' ]
   },

    { name: 'Titanium',
     photo:'aluminum',
      info:'a dark-gray or silvery, lustrous, very hard, light, corrosion-resistant, metallic element, occurring combined in various minerals: used in metallurgy to remove oxygen and nitrogen from steel and to toughen it. ',
       marking: ['anneal','etch','engrave' ] 
   },

  ];



   var marking = [
    { name: 'ablation',
     laser: ['uSeries','ufSeries','um1' ]
   },
    { name: 'anneal',
       laser: ['uSeries','ufSeries','um1' ] 
   },

    { name: 'colorChange',
      laser: ['uSeries','ugSeries','um1' ]
   },

    { name: 'foaming',
      laser: ['uSeries','ugSeries','um1' ]
   },

    { name: 'etching',
       laser: ['uSeries','ufSeries' ]
   },

    { name: 'engrave',
      laser: ['ufSeries']
   },

    { name: 'burnish',
      laser: ['ugSeries']
   },

  ];

})();

