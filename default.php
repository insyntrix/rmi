<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>

		<div id="banner-wrap">
			<div class="row">
				<h4>The Leader in Marking Lasers and Equiptment</h4>
			</div>
		</div><!--end .banner-wrap-->
		<section id="laser-experts">
			<div class="row">
				<div class="large-5 columns"></div>
				<div class="small-12 large-6 columns text-right">
					<h1>Laser Experts</h1>
					<?php
						$areaMain  = new Area('Intro Content');
						$areaMain->display($c);
					?>
				</div><!--end .sml-12-->	
			</div><!--end .row-->
		</section>
		<section id="solutionfinder">
			<div class="hide-for-small-only">
				<div class="solution-wrap">
					<div ng-controller="solutionsController"> 
						<div class="sf-header">
							<h4>Let our Solution Finder help you decide <strong>What Laser is right for you?</strong></h4>
						</div><!--end sf-header-->
			
						<div class="sf-subheader clearfix">
						</div><!--end .sf-subheader-->
			
						<div ng-include="'solutionsFinder.html'"></div>
						<div id="{{material.photo}}" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog"></div>          
					</div><!--end .solutionsController-->
				</div><!--end .solution-wrap-->
			</div><!--end .hide-for-small-->
		</section>

		<section id="ft-products">
			<div class="row">
				<div class="small-12 medium-4 columns">
					<div class="feature-wrap">
						<div class="ft-img ">
							<center>
								<img src= "<?php echo $this->getThemePath();?>/img/laser/2.png">
							</center>
						</div><!--end .ft-img-->
						<div class="ft-title">
							<h3>Laser Marking Systems</h3>
						</div><!--end .ft-title-->
						<div class="ft-content">
							<?php
								$areaMain  = new Area('featureContent1');
								$areaMain->display($c);
					        ?>
							<a class="red-btn" href="">View Lasers</a>
				</div><!--end .ft-content-->
			</div><!--end .feature-wrap-->

				</div><!--end .sml-12-->
				<div class="small-12 medium-4 columns">
					<div class="feature-wrap">
						<div class="ft-img">
							<center>
							<img src= "<?php echo $this->getThemePath();?>/img/laser/2.png">
							</center>
						</div><!--end .ft-img-->
						<div class="ft-title">
							<h3>Laser Work Systems</h3>
						</div><!--end .ft-title-->
						<div class="ft-content">
							<?php
						$areaMain  = new Area('featureContent2');
						$areaMain->display($c);
					        ?>
							<a class="red-btn" href="">View Stations</a>
						</div><!--end .ft-content-->
					</div><!--end .feature-wrap-->
				</div><!--end .sml-12-->

				
				<div class="small-12 medium-4 columns">
					<div class="feature-wrap">
						<div class="ft-img">
							<center>
							<img src= "<?php echo $this->getThemePath();?>/img/laser/2.png">
							</center>
						</div><!--end .ft-img-->
						<div class="ft-title">
							<h3>Accessories</h3>
						</div><!--end .ft-title-->
						<div class="ft-content">
							<?php
						$areaMain  = new Area('featureContent3');
						$areaMain->display($c);
					        ?>
							<a class="red-btn" href="">View Accessories</a>
						</div><!--end .ft-content-->
					</div><!--end .feature-wrap-->
				</div><!--end .sml-12-->		
			</div><!--end .row-->
		</section>


		<section id="">
			<div class="row">
				<div class="small-12 medium-4 columns">
					<div class="angle-red">
						<h4>Upcoming Eventes</h4>
					</div><!--end .angle-->
					<div class="events-wrap">
						<?php
							$areaMain  = new Area('Events');
							$areaMain->display($c);
					    ?>
					</div><!--end .events-wrap-->
				</div><!--end .sml-12 med-4-->
				<div class="small-12 medium-8 columns show-for-medium-up">
					<div class="row display">
						<div class="small-12 columns">
							<div class="angle-gray">
								<h4>Thousands of Applications</h4>
							</div>
						</div><!--end .sml-12-->
					</div><!--end .row-->
					<div class="row display">
						<div class="medium-6 columns">
							<div class="app-img-wrap">
								<span class="app-name">Stainless Steel</span>
								<a><img src= "<?php echo $this->getThemePath();?>/img/material/stainlesssteal-ft.jpg"></a>		
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="app-img-wrap">
								<span class="app-name">Aluminum</span>
								<a><img src= "<?php echo $this->getThemePath();?>/img/material/aluminium-ft.jpg"></a>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="app-img-wrap">
								<span class="app-name">Chrome</span>
								<a><img src= "<?php echo $this->getThemePath();?>/img/material/chrome-ft.jpg">	</a>
							</div>
						</div>
					</div><!--end .row-->
				</div><!--end .sml-12 med-8-->
			</div><!--end .row-->
		</section>
		
<?php $this->inc('elements/footer.php');?>