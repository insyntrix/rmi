<?php defined('C5_EXECUTE') or die(_("Access Denied."));?>
<?php $this->inc('elements/header.php'); ?>

	<div id="banner-wrap">
    <h1> <?php echo $c->getCollectionName()?></h1>
  </div>
		

<div class="row">
   <div class="small-12 medium-9 columns">
     <?php
     $areaMain  = new Area('mainContent');
     $areaMain->display($c);
     ?>
   </div>


<div class="small-12 medium-3 columns">

<div class="small-12 columns">
   <?php
    $areaMain  = new Area('sideImage');
    $areaMain->display($c);
    ?>
  </div>
    <?php
    $areaMain  = new Area('sideContent');
    $areaMain->display($c);
    ?>
  </div>
</div>

    

<?php $this->inc('elements/footer.php');?>
